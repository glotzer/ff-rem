#!/usr/bin/env python
import os
from pprint import pprint

import numpy as np
import hoomd
import hoomd.md
import gsd.hoomd
from ffrem import FFREM
from ffrem import calc_rdf

hoomd.context.initialize()

steps = 1e4
kT = 2.0


# Target Simulation:
with hoomd.context.SimulationContext():
    try:
        hoomd.init.read_gsd(filename='init.gsd', restart='restart.gsd')
    except RuntimeError:
        hoomd.init.create_lattice(hoomd.lattice.sc(a=2.0), n=8)
        hoomd.dump.gsd('init.gsd', period=None, truncate=True, group=hoomd.group.all())
    lj = hoomd.md.pair.lj(r_cut=2.5, nlist=hoomd.md.nlist.cell())
    lj.pair_coeff.set('A', 'A', epsilon=1.0, sigma=1.0)
    hoomd.md.integrate.mode_standard(dt=0.005)
    all = hoomd.group.all()
    hoomd.md.integrate.langevin(group=all, kT=kT, seed=42)
    hoomd.analyze.log('dump.log', ['temperature', 'pressure'], period=100)
    group = hoomd.group.all()
    restart = hoomd.dump.gsd('restart.gsd', period=100, group=group, truncate=True)
    hoomd.run_upto(int(0.8 * steps))
    hoomd.dump.gsd('dump.gsd', period=(0.2 * steps) // 200, group=group)
    hoomd.run_upto(int(steps))
    restart.write_restart()


# Initialize FFREM optimization:
with gsd.hoomd.open('dump.gsd') as traj:
    target = calc_rdf(traj, 'A', 'A', 3.1)
    np.savetxt('target.txt', target.T)
    ffrem = FFREM(target, r_min=2.4, r_max=3.0, beta=1.0/kT)
    print("Parameter used for this run:")
    pprint(ffrem.to_dict())


# Execute FFREM iterations:
fitness = []
delta = []
for i in range(20):

    # Create directory for current iteration prefix.
    prefix = os.path.join('iteration', str(i))
    os.makedirs(prefix, exist_ok=True)

    def prefixed(filename):
        "Prepend filename with current iteration prefix."
        return os.path.join(prefix, filename)

    with hoomd.context.SimulationContext():
        hoomd.init.read_gsd(filename='init.gsd', restart=prefixed('restart.gsd'))

        # Dump k-space and real-space potential to disk.
        np.savetxt(prefixed('k_potential.txt'), ffrem.k_potential().T)
        potential_table = ffrem.potential()
        np.savetxt(prefixed('potential.txt'), potential_table.T)

        # Setup tabulated real-space potential as pairwise interaction potential.
        table = hoomd.md.pair.table(width=len(potential_table[0]), nlist=hoomd.md.nlist.cell())
        table.set_from_file('A', 'A', filename=prefixed('potential.txt'))

        # Setup langevin integrator for integration.
        hoomd.md.integrate.mode_standard(dt=0.005)
        all = hoomd.group.all()
        hoomd.md.integrate.langevin(group=all, kT=kT, seed=42)

        # Setup trajectory dumping.
        restart = hoomd.dump.gsd(prefixed('restart.gsd'), period=steps//100, group=all, truncate=True)
        hoomd.dump.gsd(prefixed('dump.gsd'), period=steps//200, group=all)

        # Sample for 'steps' steps.
        hoomd.run_upto(steps)

        # Load trajectory and compute radial distribution function (RDF) for update.
        with gsd.hoomd.open(prefixed('dump.gsd')) as traj:
            try:
                response = np.loadtxt(prefixed('response.txt')).T
            except OSError:
                response = calc_rdf(traj[int(0.8 * len(traj)):], 'A', 'A', 3.1)
                np.savetxt(prefixed('response.txt'), response.T)

        # Perform FFREM update and optionally break when converged.
        delta.append(ffrem.update(response))
        fitness.append(ffrem.fitness())

        np.savetxt('delta.txt', delta)
        np.savetxt('fitness.txt', fitness)

        print("Fitness at iteration {}: {:0.3f}.".format(i+1, fitness[-1]))
        if delta[-1] < 0.1:
            print("Converged after {} iterations.".format(i+1))
            break   # converged
else:
    raise RuntimeError("Failed to converge.")
