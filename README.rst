==============================================
Fourier-filtered Relative Entropy Minimization
==============================================


.. image:: https://img.shields.io/pypi/v/ff-rem.svg
        :target: https://pypi.python.org/pypi/ff-rem

.. image:: https://readthedocs.org/projects/ff-rem/badge/?version=latest
        :target: https://ff-rem.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


Implementation of the Fourier-filtered Entropy Minimization (FF-REM) method for HOOMD-blue.
The method is described in detail in the associated publication:

    Carl S. Adorf, James Antonaglia, Julia Dshemuchadse, Sharon C. Glotzer, 2018. DOI: `10.1063/1.5063802 <https://dx.doi.org/10.1063/1.5063802>`_.

* Free software: MIT license
* Documentation: https://ff-rem.readthedocs.io.

Quickstart
----------

A complete example for the recovery of a Lennard-Jones potential is shown in ``examples/lennard-jones``.


Requirements
------------

* numpy
* HOOMD-blue
* gsd

Testing
-------

To execute unit tests, run:

.. code-block:: bash

    $ python -m unittest discover tests/
    
within the package root directory.

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
