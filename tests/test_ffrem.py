#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `ffrem` package."""
import os
import tempfile
import unittest

import hoomd
import hoomd.md
import gsd.hoomd
import numpy as np

from ffrem import FFREM
from ffrem import calc_rdf
from data import TARGET_LJ_KT2_N8


hoomd.context.initialize()
hoomd.option.set_notice_level(0)


class TestFFREM(unittest.TestCase):
    """Tests for `ffrem` package."""

    def setUp(self):
        """Set up test fixtures, if any."""
        self._tmp_dir = tempfile.TemporaryDirectory(prefix='ffrem_')
        self.addCleanup(self._tmp_dir.cleanup)

    def test_000_construct_ffrem_instance(self):
        """Test the construction of the FFREM class."""
        ffrem = FFREM(TARGET_LJ_KT2_N8, r_min=2.4, r_max=3.0, beta=1.0)
        self.assertEqual(ffrem.iteration, -1)

    def test_001_test_ffrem_fitness(self):
        """Test basic function of the FFREM fitness function."""
        ffrem = FFREM(TARGET_LJ_KT2_N8, r_min=2.4, r_max=3.0, beta=1.0)
        self.assertEqual(ffrem.iteration, -1)
        ffrem.update(TARGET_LJ_KT2_N8)
        self.assertEqual(ffrem.iteration, 0)
        self.assertEqual(ffrem.fitness(), 1.0)

    def test_002_fluid_lennard_jones_model(self):
        """Test whether FFREM is able to approximate the Lennard-Jones potential for a fluid.

        Adapted from examples/lennard-jones.
        """
        # Simulation parameters for test optimization run
        steps = 1e4
        kT = 2.0

        # Setup ffrem instance.
        ffrem = FFREM(TARGET_LJ_KT2_N8, r_min=2.4, r_max=3.0, beta=1.0/kT)

        # Create initial configuration.
        with hoomd.context.SimulationContext():
            hoomd.init.create_lattice(hoomd.lattice.sc(a=2.0), n=8)
            hoomd.dump.gsd(
                filename=os.path.join(self._tmp_dir.name, 'init.gsd'),
                period=None, truncate=True, group=hoomd.group.all())

        # Perform the FF-REM optimization:
        for i in range(20):
            self.assertEqual(ffrem.iteration, i-1)

            def prefixed(filename):
                return os.path.join(self._tmp_dir.name, str(i), filename)

            os.makedirs(prefixed(''), exist_ok=True)

            with hoomd.context.SimulationContext():
                hoomd.init.read_gsd(filename=os.path.join(self._tmp_dir.name, 'init.gsd'))
                potential_table = ffrem.potential()

                np.savetxt(prefixed('potential.txt'), potential_table.T)
                # Setup tabulated real-space potential as pairwise interaction potential.
                table = hoomd.md.pair.table(width=len(potential_table[0]), nlist=hoomd.md.nlist.cell())
                table.set_from_file('A', 'A', filename=prefixed('potential.txt'))

                # Setup langevin integrator for integration.
                hoomd.md.integrate.mode_standard(dt=0.005)
                all = hoomd.group.all()
                hoomd.md.integrate.langevin(group=all, kT=kT, seed=42)

                # Setup trajectory dumping.
                hoomd.dump.gsd(prefixed('dump.gsd'), period=steps//200, group=all)

                # Sample for 'steps' steps.
                hoomd.run_upto(steps)

                # Load trajectory and compute radial distribution function (RDF) for update.
                with gsd.hoomd.open(prefixed('dump.gsd')) as traj:
                    try:
                        response = np.loadtxt(prefixed('response.txt')).T
                    except OSError:
                        response = calc_rdf(traj[int(0.8 * len(traj)):], 'A', 'A', 3.1)
                        np.savetxt(prefixed('response.txt'), response.T)

                # Perform FFREM update and optionally break when converged.
                if ffrem.update(response) < 0.1:
                    break   # converged
                self.assertEqual(ffrem.iteration, i)
        else:
            raise RuntimeError("Failed to converge after {} iterations!".format(i+1))

        # Calculate normed difference between the expected (actual) and the approximated potential.
        r, lj_approx, _ = ffrem.potential()
        lj_actual = 4 * (np.power(r, -12) - np.power(r, -6))
        difference = np.linalg.norm(lj_actual[r > 0.9] - lj_approx[r > 0.9]) / len(r[r > 0.9])
        self.assertLess(difference, 0.02)
