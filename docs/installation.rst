.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Fourier-filtered Relative Entropy Minimization, run this command in your terminal:

.. code-block:: console

    $ pip install ff-rem

This is the preferred method to install Fourier-filtered Relative Entropy Minimization, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Fourier-filtered Relative Entropy Minimization can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://bitbucket.org/glotzer/ff-rem

Or download the `tarball`_:

.. code-block:: console

    $ curl -Ol https://bitbucket.org/glotzer/ff-rem/get/master.zip

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _bitbucket repo: https://bitbucket.org/glotzer/ff-rem
.. _tarball: https://bitbucket.org/glotzer/ff-rem/get/master.zip
